﻿const config = require('config.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const moment = require('moment');
const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    getAudits,
    authenticateLoggedInUserRole,
    delete: _delete
};

async function authenticate({username,password,roleName,ipInfo}) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const queryToUpdate={
            loggedInUserIp: ipInfo,
            loggedInDate: moment.utc().format("MM/DD/YYYY")
        }
        await User.updateOne({username},queryToUpdate)
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id,roleName }, config.secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function authenticateLoggedInUserRole({headers,body,query},res,next){
    var authToken = headers['x-auth-token'] || body.authToken || query.token;
    if (!authToken) {
        return false
    }
    
    //verifies secret and checks exp
    jwt.verify(authToken, config.secret,async function (err, decoded) {
        if (err) {
            return false
        }

        if (!decoded.roleName) {
            return false
        }

        //If  AUDITOR role
        decoded.roleName == 'AUDITOR' ? true : false

    })
}
    




async function getAll() {
    return await User.find().select('-hash');
}

async function getAudits() {
    return await User.find();
   
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}